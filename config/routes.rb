Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  resources :leaderboard, :only => [:index]

  get 'update_color', to: 'dashboard#update_color'
  get 'fav_colors', to: 'leaderboard#fav_colors'

  root 'dashboard#index'

end
