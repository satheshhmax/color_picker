class CreateMatrices < ActiveRecord::Migration[5.1]
  def change
    create_table :matrices do |t|
      t.integer    :row
      t.integer    :column
      t.string     :color
      t.references :user,    foreign_key: true

      t.timestamps
    end
  end
end
