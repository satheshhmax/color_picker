require 'rails_helper'

RSpec.describe Matrix, type: :model do
  describe 'db' do
    context 'indexes' do
      it { is_expected.to have_db_index(:user_id) }
    end

    context 'columns' do
      it { is_expected.to have_db_column(:row).of_type(:integer) }
      it { is_expected.to have_db_column(:column).of_type(:integer) }
      it { is_expected.to have_db_column(:color).of_type(:string) }
      it { is_expected.to have_db_column(:user_id).of_type(:integer) }
      it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
      it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    end
  end

  describe 'update_color' do
    let(:valid_color) { '#000000' }
    let(:invalid_color) { '' }
    let(:matrix) { create(:matrix) }
    let(:matrix_without_color) { create(:matrix_without_color) }

    context 'update_color for matrix with' do
      it 'valid_params is success' do
        user = create(:user)
        expect(matrix.update_color(user.id, valid_color)).to be true
        expect(matrix).to have_attributes(color: '#000000', user_id: user.id)
      end
    end

    context 'update_color for matrix_without_color with' do
      it 'valid_params is success' do
        user = create(:user)
        expect(matrix_without_color.update_color(user.id, valid_color)).to be true
        expect(matrix_without_color).to have_attributes(color: '#000000', user_id: user.id)
      end
    end

    context 'update_color with' do
      it 'invalid color and user should fail' do
        user = create(:user)
        expect(matrix.update_color(user.id, nil)).to be false
        expect(matrix.update_color(nil, nil)).to be false
        expect(matrix.update_color(nil, valid_color)).to be false
      end
    end
  end

  describe 'update_color' do
    let(:matrix) { create(:matrix) }

    context 'display_title for matrix' do
      it 'have user name and updated_at' do
        expect(matrix.display_title).to be_eql("#{matrix.user.name.camelcase}, #{matrix.updated_at.strftime('%d/%m/%y %H:%M %p')}")
      end
    end
  end

  describe 'scopes' do
    context 'top_users' do
      it 'return user1 in top' do
        user1 = create(:user)
        user2 = create(:user)
        create(:matrix, user: user1)
        create(:matrix, user: user1)
        create(:matrix, user: user2)

        expect(Matrix.top_users.first).to have_attributes(user_id: user1.id)
        expect(Matrix.top_users.last).to have_attributes(user_id: user2.id)
      end
    end

    context 'by_fav_colors' do
      it 'return user1' do
        user1 = create(:user)
        user2 = create(:user)
        create(:matrix, user: user1)
        create(:matrix, user: user1)
        create(:matrix, user: user1)
        create(:matrix, user: user2)

        expect(user1.matrices.by_fav_colors.length).to eq(3)
        expect(user2.matrices.by_fav_colors.length).to eq(1)
      end
    end
  end
end
