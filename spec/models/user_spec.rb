require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'db' do
    context 'columns' do
      it { is_expected.to have_db_column(:name).of_type(:string) }
      it { is_expected.to have_db_column(:current_ip).of_type(:string) }
      it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
      it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    end
  end
  context 'associations' do
    it { is_expected.to have_many(:matrices) }
  end
  context 'create record' do
    subject { create(:user) }

    it { is_expected.to be_valid }
  end
end
