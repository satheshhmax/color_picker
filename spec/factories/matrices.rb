FactoryBot.define do
  factory :matrix do
    color { Faker::Color.hex_color }
    row  1
    column 1
    association :user, factory: :user, strategy: :build

    factory :matrix_without_color do
      color { nil }
    end
  end

end
