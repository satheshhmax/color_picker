FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    current_ip { Faker::Internet.ip_v4_address }

    trait :no_current_ip do
      current_ip nil
    end

    trait :no_name do
      name nil
    end
  end

  factory :user1, class: User do
    name { Faker::Name.name }
    current_ip { Faker::Internet.ip_v4_address }
  end

  factory :user2, class: User do
    name { Faker::Name.name }
    current_ip { Faker::Internet.ip_v4_address }
  end
end
