require 'rails_helper'

RSpec.describe LeaderboardController, type: :controller do

  let(:user) { create(:user) }

  describe 'GET #index' do
    it 'render index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'GET #fav_colors' do
    it 'render fav_colors template' do
      get :fav_colors, xhr: true, params: {user_id: user.id}
      expect(response).to render_template('fav_colors')
    end
  end

end
