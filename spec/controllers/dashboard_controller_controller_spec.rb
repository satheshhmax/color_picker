require 'rails_helper'

RSpec.describe DashboardController, type: :controller do

  let(:matrix) { create(:matrix) }

  describe 'GET #index' do
    it 'index status 200' do
      get :index
      expect(assigns(:matrices)).to eq([matrix])
    end

    it 'render index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'GET #update_color' do
    let(:user) { create(:user) }

    it 'update_color status 200' do
      session[:color] = ''
      session[:user_id] = user.id
      get :update_color, params: {row: matrix.row, column: matrix.column, color: '#000000'}

      expect(response).to have_http_status(:ok)
      expect(matrix.reload).to have_attributes(color: '#000000')
      # expect(response).to have_broadcasted_to("matrix_channel").
      #                       with({row: matrix.row, column: matrix.column, color: matrix.color, title: matrix.display_title})
    end

    it 'update_color status 422' do
      session[:color] = ''
      session[:user_id] = user.id
      get :update_color, params: {row: matrix.row, column: matrix.column}

      expect(response).to have_http_status(:unprocessable_entity)
      expect(matrix.reload).to have_attributes(color: matrix.color)
    end

    it 'update_color status 404' do
      get :update_color, params: {row: nil, column: nil}
      expect(response).to have_http_status(:not_found)
    end

  end
end
