App.matrix = App.cable.subscriptions.create("MatrixChannel", {
  received: function(data) {
    var row = data['row'];
    var column = data['column'];
    var tdElemen = $("td[row='" + row + "'][column='" + column + "']");
    tdElemen.attr('bgcolor', data['color']);
    tdElemen.attr('title', data['title']);
  }
});
