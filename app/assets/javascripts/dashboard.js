$(document).on('turbolinks:load', function() {
  $('.demo').each( function() {
    $(this).minicolors({
      theme: 'bootstrap'
    });
  });
});

$(document).on("click", ".boxContainer", function (e) {
  var colorVal = $('#colorPicker').val();
  $.ajax({
    url: '/update_color',
    type: 'GET',
    dataType: 'script',
    data: {color: colorVal,
           row: $(this).attr('row'),
           column: $(this).attr('column')},
    success: function(response) {
      $(this).attr('bgcolor', colorVal);
      $(this).attr('title', $('.userName').text());
    },
    error: function(){
      alert('Invalid Data');
    }
  });
});
