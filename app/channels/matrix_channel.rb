class MatrixChannel < ApplicationCable::Channel
  def subscribed
    stream_from "matrix_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
