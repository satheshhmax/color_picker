class User < ApplicationRecord

  # CONSTANTS
  FIRST_NAME = %w(Objects Ruler Apparel Warm Cerium Every Jeans Spend Gogs Rounding Tophat Objectives Protective Box Funding Wizzed Trojan Prawns Quails Smile Lidar Mission Wavelength Dreary Glistering Apus Fizzy Sandbox Pestle Crookes Moaning Windows Barbarous Nosy Humbing Hopes Bummage Speaker Damned Hear Modest Presidents Passive Shrawns Jog Moustache Tale Weary Fags Divergent)
  LAST_NAME  = %w(Grumpy Bike Pedagogy Hopper Sname Clast Rat Puffs Tony Network Salt Frightened Mammal Reclusive Castration Cauliflower Flared Wedge Honky Southern Pimento Tekton Geiger Vomiting Flap Bubbly Kidneyed Peeler Sarrott Ascii Picket Surly Invention Catalysts Milky Uranium Shamb Tutors Mangled Listless Welcome Template Bunsen Gneeth Prepend Sheriff Realtor Fallen Fishy Karst)

  #ASSOCIATIONS
  has_many :matrices

  # CALLBACK
  before_create :set_default_values

  private

  def set_default_values
    self.name = [FIRST_NAME.sample, LAST_NAME.sample].join(' ')
  end
end
