class Matrix < ApplicationRecord

  #ASSOCIATIONS
  belongs_to :user, optional: true

  # VALIDATIONS
  validates_format_of :color, with: /#?([A-F0-9]{3}){1,2}/i, on: :update
  validates_presence_of :user_id, on: :update

  scope :top_users, -> { select('user_id, color, count(DISTINCT(color)) AS distinct_color_count,
                                 count(color) AS color_count').
                         where('user_id IS NOT NULL').
                         group(:user_id).
                         includes(:user).
                         order('distinct_color_count DESC, color_count DESC') }

  scope :by_fav_colors, -> { select("color, count(*) as count").
                             order('count DESC').
                             group(:color) }

  def update_color user_id, color
    update_attributes(user_id: user_id, color: color)
  end

  def display_title
    "#{user.name.camelcase}, #{updated_at.strftime('%d/%m/%y %H:%M %p')}"
  end
end
