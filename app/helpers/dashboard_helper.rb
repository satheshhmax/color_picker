module DashboardHelper

  def td_content_tag matrix
    content_tag(:td, '', title: (matrix.display_title if matrix.user.present?),
                row: matrix.row, column: matrix.column,
                bgcolor: matrix.color, class: 'boxContainer')
  end

  def td_fav_color_tag fav_color
    content_tag(:td, fav_color.count, bgcolor: fav_color.color,
                class: 'favColorContainer')
  end

  def current_user_color
    session[:color] || random_color
  end

  def random_color
    "%06x" % (rand * 0xffffff)
  end
end
