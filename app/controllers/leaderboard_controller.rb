class LeaderboardController < ApplicationController

  before_action :for_user, only: [:fav_colors]

  def index
    @top_users = Matrix.top_users
  end

  def fav_colors
    @fav_colors = @for_user.matrices.by_fav_colors
  end

  private

  def for_user
    @for_user = User.where(id: params[:user_id]).first
  end
end
