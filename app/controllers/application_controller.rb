class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_user

  private

  def set_user
    if session[:user_id]
      @current_user = User.where(id: session[:user_id]).first
    else
      # For testing with users from same ip, commenting IP check below.
      # @current_user = User.where(current_ip: request.remote_ip).first_or_create
      @current_user = User.create
      session[:user_id] = @current_user.id
    end
  end
end
