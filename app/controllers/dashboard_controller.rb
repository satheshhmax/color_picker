class DashboardController < ApplicationController

  before_action :update_color_session, only: [:update_color]
  before_action :set_matrix, only: [:update_color]

  def index
    @matrices = Matrix.all.includes(:user)
  end

  def update_color
    if @matrix.update_color(session[:user_id], params[:color])
      ActionCable.server.broadcast "matrix_channel",
                                   { row: @matrix.row,
                                     column: @matrix.column,
                                     color: @matrix.color,
                                     title: @matrix.display_title }
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def update_color_session
    session[:color] = params[:color]
  end

  def set_matrix
    @matrix = Matrix.where(row: params[:row], column: params[:column]).first
    head :not_found unless @matrix
  end
end
